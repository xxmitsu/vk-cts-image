#!/bin/bash

BASE_URL="https://gitlab.freedesktop.org/api/v4/projects/176/merge_requests"

if [ $# != 2 ]; then
    echo "$0 <merge_request_id> <gpu_family>"
    exit 1
fi

set -ex

id=$1
gpu_family=$2

tmp=$(mktemp /tmp/tmpXXXXXX)
curl "$BASE_URL/$id" > $tmp
branch=$(cat $tmp | python -c "import sys, json; print(json.load(sys.stdin)['source_branch'])")
username=$(cat $tmp | python -c "import sys, json; print(json.load(sys.stdin)['author']['username'])")
rm $tmp

./run-cts.sh -r $username -b $branch -g $gpu_family
./run-fossilize.sh -r $username -b $branch --compare-master
