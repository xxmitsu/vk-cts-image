#!/bin/bash

TEST_IMAGE=$(cat IMAGE_VERSION)

result_dir=`pwd`/results
mesa_rebase_branch=1
fossilize_compare_master=0
fossilize_last_n_commit=1
compiler="aco"
gpu_device_id=0

while getopts "b:c:d:j:n:o:r:-:" OPTION; do
    case $OPTION in
    -)
        case "${OPTARG}" in
            compare-master)
                fossilize_compare_master=1
                ;;
            *)
                if [ "$OPTERR" = 1 ] && [ "${optspec:0:1}" != ":" ]; then
                    echo "Unknown option --${OPTARG}" >&2
                fi
                exit 0
                ;;
        esac;;
    b)
        mesa_branch=$OPTARG
        ;;
    c)
        compiler=$OPTARG
        ;;
    d)
        gpu_device_id=$OPTARG
        ;;
    j)
        num_threads=$OPTARG
        ;;
    n)
        fossilize_last_n_commit=$OPTARG
        ;;
    o)
        result_dir=$OPTARG
        ;;
    r)
        mesa_remote=$OPTARG
        ;;
    *)
        echo "Incorrect options provided"
        exit 1
        ;;
    esac
done

if [ -z $mesa_remote ] || [ -z $mesa_branch ]; then
    echo "$0 -r <mesa_remote> -b <mesa_branch> [-d <gpu_device_id>] [-j <fossilize_parallel>] [-n <last_n_commit>] [-o <result_dir>]"
    exit 1
fi

if [ "$compiler" != "aco" ] && [ "$compiler" != "llvm" ]; then
    echo "Invalid compiler option (accepted values are: aco, llvm)"
    exit 1
fi

# Enable LLVM via RADV_DEBUG if set.
radv_debug=""
if [ "$compiler" == "llvm" ]; then
    radv_debug="llvm,checkir"
fi

if [ -z $num_threads ]; then
    num_threads=$(nproc)
fi

set -ex

mkdir -p $result_dir

# Pull the latest image.
docker pull $TEST_IMAGE

# Build a specific Mesa remote/branch and run CTS with deqp-runner.
docker run \
    --device /dev/dri/renderD$((128+$gpu_device_id)) \
    --mount src=`pwd`/testing,target=/mnt/testing,type=bind \
    --mount src=`pwd`/external/mesa,target=/mnt/mesa,type=bind \
    --mount src=`pwd`/external/radv_fossils,target=/mnt/radv_fossils,readonly,type=bind \
    --mount src=$result_dir,target=/mnt/results,type=bind \
    --network host \
    --security-opt label:disable \
    --env MESA_SOURCE_REMOTE=$mesa_remote \
    --env MESA_SOURCE_BRANCH=$mesa_branch \
    --env MESA_REBASE_BRANCH=$mesa_rebase_branch \
    --env FOSSILIZE_COMPARE_MASTER=$fossilize_compare_master \
    --env FOSSILIZE_LAST_N_COMMIT=$fossilize_last_n_commit \
    --env NUM_THREADS=$num_threads \
    --env RADV_DEBUG=$radv_debug \
    --env-file env \
    -it $TEST_IMAGE \
    bin/bash /mnt/testing/run-fossilize.sh
