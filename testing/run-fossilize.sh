#!/bin/bash

set -ex

RESULTS_DIR="/mnt/results"

get_output_file()
{
    # Get the sha1 of the current commit.
    pushd /mnt/mesa > /dev/null
    sha1=$(git rev-parse --short $MESA_SOURCE_REMOTE/$MESA_SOURCE_BRANCH~$MESA_SOURCE_HEAD)
    popd > /dev/null

    # Build the statistics output filename.
    echo "$RESULTS_DIR/fossilize-$MESA_SOURCE_REMOTE-$MESA_SOURCE_BRANCH-$sha1.csv"
}

run_fossilize()
{
    output_file=$1

    export LD_LIBRARY_PATH=/mnt/mesa/install/lib/:/usr/local/lib
    export VK_ICD_FILENAMES=/mnt/mesa/install/share/vulkan/icd.d/"$VK_DRIVER"_icd.x86_64.json

    if [ -n "$NUM_THREADS" ]; then
        FOSSILIZE_PARALLEL="--num-threads $NUM_THREADS"
    fi

    /usr/local/bin/fossil_replay.sh /mnt/radv_fossils/fossils \
        $output_file $FOSSILIZE_PARALLEL
}

# Run fossilize against the last N commit (1 is the default which means it only
# test the top commit on the source remote/branch).
output_files=()
for (( i=0; i<$FOSSILIZE_LAST_N_COMMIT; i++ ))
do
    # Run Fossilize against the source remote/branch.
    export MESA_SOURCE_HEAD=$i
    /mnt/testing/build-mesa.sh
    dev_db=$(get_output_file)
    run_fossilize $dev_db

    # Add the output file to the list for later comparisons.
    output_files=($dev_db ${output_files[@]})
done

if [ $FOSSILIZE_COMPARE_MASTER -eq 1 ]; then
    # If we want to report the Fossilize results, build the origin/master
    # branch. This assumes that the source branch has been rebased first.
    export MESA_SOURCE_REMOTE=origin
    export MESA_SOURCE_BRANCH=master
    export MESA_REBASE_BRANCH=0
    export MESA_SOURCE_HEAD=0

    /mnt/testing/build-mesa.sh
    baseline_db=$(get_output_file)
    run_fossilize $baseline_db

    # Add the output file to the list for later comparisons.
    output_files=($baseline_db ${output_files[@]})
fi

set +x

# Report Fossilize results.
len=${#output_files[@]}
if [ $len > 1 ]; then
    for (( i=0; i<$len-1; i++ ))
    do
        before=${output_files[$i]}
        after=${output_files[$i+1]}

        set -ex
        /usr/local/bin/radv-report-fossil.py $before $after
        set +x
    done
fi
