#!/bin/bash

set -ex

pushd /mnt/mesa

# If a previous rebase failed for some reasons, make sure to abort it.
if [ -d .git/rebase-apply ]; then
    rm -rf .git/rebase-apply
fi

# Clean up the repo.
git clean -fdx .
git reset --hard
git checkout master

# Add the source remote if unknown.
set +e
remote=`git remote | grep -w $MESA_SOURCE_REMOTE`
set -ex
if [ -z $remote ]; then
    git remote add $MESA_SOURCE_REMOTE https://gitlab.freedesktop.org/$MESA_SOURCE_REMOTE/mesa.git
fi

# Fetch the source remote and checkout the branch.
git fetch $MESA_SOURCE_REMOTE
git checkout $MESA_SOURCE_REMOTE/$MESA_SOURCE_BRANCH

# Checkout the commit HEAD-n (0 is the default and the top commit).
git checkout HEAD~$MESA_SOURCE_HEAD

# Rebase the source branch on the origin/master branch if requested.
if [ $MESA_REBASE_BRANCH -eq 1 ]; then
    git fetch origin
    git rebase origin/master
fi

# Build the branch.
.gitlab-ci/meson-build.sh

# Prepare artifacts for the dEQP runner.
FILES=.gitlab-ci/deqp-radv-*.txt
for f in $FILES
do
   sed -e 's/,.*$//g' $f > install/$(basename $f)
done

popd
