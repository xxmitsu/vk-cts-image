#!/bin/bash

set -ex

/mnt/testing/build-mesa.sh
/mnt/testing/check-device.sh

pushd /mnt/mesa

set +e

export DEQP_PARALLEL=$NUM_THREADS

/mnt/testing/deqp-runner.sh

if [ $? -ne 0 ]; then
    # Save the list of unexpected failures.
    cp /mesa/results/cts-runner-unexpected-results.txt /mnt/results
fi

popd
