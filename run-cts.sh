#!/bin/bash

TEST_IMAGE=$(cat IMAGE_VERSION)

result_dir=`pwd`/results
compiler="aco"
mesa_rebase_branch=1
gpu_device_id=0
gpu_families=(pitcairn
              fiji
              polaris10
              raven
              vega10
              navi10
              navi14
              sienna_cichlid)

while getopts "hb:c:d:g:j:o:r:-:" OPTION; do
    case $OPTION in
    -)
        case "${OPTARG}" in
            no-rebase)
                mesa_rebase_branch=0
                ;;
            *)
                if [ "$OPTERR" = 1 ] && [ "${optspec:0:1}" != ":" ]; then
                    echo "Unknown option --${OPTARG}" >&2
                fi
                exit 0
                ;;
        esac;;
    h)
        echo -n "$0 -r <source_remote> -b <source_branch> -g <gpu_family> "
        echo "[ -j <num_threads> -c <aco|llvm> -o <result_dir> --no-rebase ]"
        exit 0
        ;;
    b)
        mesa_branch=$OPTARG
        ;;
    c)
        compiler=$OPTARG
        ;;
    d)
        gpu_device_id=$OPTARG
        ;;
    g)
        gpu_family=$OPTARG
        ;;
    j)
        num_threads=$OPTARG
        ;;
    o)
        result_dir=$OPTARG
        ;;
    r)
        mesa_remote=$OPTARG
        ;;
    *)
        if [ "$OPTERR" != 1 ] || [ "${optspec:0:1}" = ":" ]; then
            echo "Non-option argument: '-${OPTARG}'" >&2
        fi
        exit 1
        ;;
    esac
done

if [ -z $mesa_remote ] || [ -z $mesa_branch ] || [ -z $gpu_family ]; then
    echo "$0 -r <mesa_remote> -b <mesa_branch> -g <gpu_family> [-d <gpu_device_id>] [-j <deqp_parallel>] [-d <result_dir>] [-c <compiler>]"
    exit 1
fi

found=0
for g in "${gpu_families[@]}"
do
    if [ "$g" == "$gpu_family" ] ; then
        found=1
    fi
done

if [ $found -eq 0 ]; then
    echo "Unsupported GPU family option (accepted values: ${gpu_families[@]})"
    exit 1
fi

if [ "$compiler" != "aco" ] && [ "$compiler" != "llvm" ]; then
    echo "Invalid compiler option (accepted values are: aco, llvm)"
    exit 1
fi

# Default list of skipped/expected tests.
deqp_skips="deqp-radv-default-skips.txt"
deqp_expected_failures=""

# Select the list of expected failures (only supported for ACO).
if [ "$compiler" == "aco" ]; then
    deqp_expected_fails="deqp-radv-$gpu_family-aco-fails.txt"
fi

# Select a different of list of skipped tests for RAVEN to avoid GPU hangs.
if [ "$gpu_family" == "raven" ]; then
    deqp_skips="deqp-radv-raven-aco-skips.txt"
fi

# Enable LLVM via RADV_DEBUG if set.
radv_debug=""
if [ "$compiler" == "llvm" ]; then
    radv_debug="llvm,checkir"
fi

if [ -z $num_threads ]; then
    num_threads=$(nproc)
fi

set -ex

mkdir -p $result_dir

# Pull the latest image.
docker pull $TEST_IMAGE

# Build a specific Mesa remote/branch and run CTS with deqp-runner.
docker run \
    --device /dev/dri/renderD$((128+$gpu_device_id)) \
    --mount src=`pwd`/testing,target=/mnt/testing,type=bind \
    --mount src=`pwd`/external/mesa,target=/mnt/mesa,type=bind \
    --mount src=$result_dir,target=/mnt/results,type=bind \
    --network host \
    --security-opt label:disable \
    --env MESA_SOURCE_REMOTE=$mesa_remote \
    --env MESA_SOURCE_BRANCH=$mesa_branch \
    --env MESA_SOURCE_HEAD=0 \
    --env MESA_REBASE_BRANCH=$mesa_rebase_branch \
    --env NUM_THREADS=$num_threads \
    --env EXPECTED_GPU_FAMILY=$gpu_family \
    --env DEQP_SKIPS=$deqp_skips \
    --env DEQP_EXPECTED_FAILS=$deqp_expected_fails \
    --env RADV_DEBUG=$radv_debug \
    --env-file env \
    -it $TEST_IMAGE \
    bin/bash /mnt/testing/run-cts.sh
